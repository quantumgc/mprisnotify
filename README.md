# MPRIS-Notify

MPRIS-Notify is a script that uses `pydbus` to present
information about the currently playing track as a notification.


# Requirements

+ Python >=3.6
+ A music player with MPRIS support available and installed
+ A notification server that integrates with DBUS

# Acknowledgments

This is heavily influenced by the [mpdnotify](https://github.com/vehk/mpdnotify)
script written by Wolfgang Müller.
